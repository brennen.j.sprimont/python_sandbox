#!/usr/bin/python
"""
Sandbox messing around with the python event scheduler 'sched' module.
"""
import sys
import os
import time
import sched
import logging



def relative_event_scheduling_test():
    """ Test messing around with the 'sched' event scheduling module.
    """
    logging.info("Starting event scheduling test...")
    my_scheduler = sched.scheduler(time.time, time.sleep)
    # Perform a one time event after 5 seconds.
    my_scheduler.enter(5, 1, dummy_example_event)
    # Perform a recurring event every 2 seconds.
    my_scheduler.enter(2, 1, dummy_example_event_recurring, argument=([my_scheduler, 2]))
    my_scheduler.run()


def dummy_example_event():
    """ One time fire dummy example event.
    """
    logging.info("Firing dummy example event")
    # Do real work here.

def dummy_example_event_recurring(my_scheduler, time_interval):
    """ Reoccuring dummy example event.

    By passing the scheduler into the event, we can add to the scheduler to re-fire this
    event. 

    :param sched.scheduler my_scheduler: The scheduler to re-attach this event to.
    :param int  time_interval: The interval to wait before performing this event again in seconds.
    """
    logging.info("Firing recurring dummy example event")
    # Do real work here.
    if my_scheduler is not None:
        my_scheduler.enter(2, 1, dummy_example_event_recurring, argument=([my_scheduler, time_interval]))
        my_scheduler.run()
    else:
        logging.error("No scheduler passed in! Cannot reschedule the event!")
        # Create a custom exception and raise it here if we want to be fancy.


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s|%(levelname)s|%(filename)s:%(lineno)s|%(funcName)s|%(message)s", stream=sys.stdout, level=logging.DEBUG)
    relative_event_scheduling_test()