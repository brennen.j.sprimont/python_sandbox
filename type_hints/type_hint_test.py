

# PEP 484
# https://www.python.org/dev/peps/pep-0484


def alternative_math(my_string: str, my_number: int) -> str:
    my_string = my_string + str(my_number)
    return my_string

if __name__ == "__main__":
    my_name = alternative_math("my words", 5)
    print(my_name)

    